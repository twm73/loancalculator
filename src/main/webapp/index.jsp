<%@page language="java" contentType="text/html; ISO-8859-1" pageEncoding="UTF-8" %>
<!DOCTYPE <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="pl-PL">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Loan calculator</title>
	<style type="text/css">
    	.right { text-align: right; } </style>

</head>
<body>
<FORM action="symulacja" method="post">
    <h2>Kalkulator kredytowy</h2>
    <p>Podaj kwotę, którą chcesz pożyczyć:</p>
    <p><input type='number' name='kwota_kredyt' maxlength='7' class='right' value=10000> zł</p>

    <p>Podaj kwotę opłaty stałej:</p>
    <p><input type='number' name='oplata_stala' maxlength='5' class='right' value=200> zł</p>
    <p>
        Wybrałeś oprocentowanie nominalne: <span id="procent"></span>%.</br>
        5 <input type='range' id='oprocentowanie_kredyt' name='oprocentowanie_kredyt' class='right' min=5 max=15 value=12> 15%
	</p>
    <p>
    	Wybrałeś <span id="raty"></span> rat.<br>
        0 <input type='range' id='raty_kredyt' name='raty_kredyt' class='right' min=6 max=60 value=12> 60
	</p>
    <p>
    	<input type="radio" name="splata" checked value="stale">Stałe
        <input type="radio" name="splata" value="malejace">Malejące
	</p>


    <input type ="submit" value="Przelicz">

</FORM>
</body>
	<script>
	    var sRaty = document.getElementById("raty_kredyt");
	    var oRaty = document.getElementById("raty");
	    oRaty.innerHTML = sRaty.value;
	    var sProc = document.getElementById("oprocentowanie_kredyt");
	    var oProc = document.getElementById("procent");
	    oProc.innerHTML = sProc.value;
	
	    sRaty.oninput = function() {oRaty.innerHTML = this.value;};
	    sProc.oninput = function() {oProc.innerHTML = this.value;}
	
	</script>
</html>