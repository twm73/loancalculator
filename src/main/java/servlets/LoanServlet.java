package servlets;

import logic.InstallmentCalculator;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoanServlet
 */
@WebServlet("/symulacja")
public class LoanServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

	    if (
                request.getParameter("kwota_kredyt") == null
	            || request.getParameter("kwota_kredyt").equals("")
                || request.getParameter("kwota_kredyt").equals("0")
                || request.getParameter("oplata_stala") == null
                || request.getParameter("oplata_stala").equals("")
                ){response.sendRedirect("/");}

        String type =  request.getParameter("splata");
        InstallmentCalculator installmentCalculator = new InstallmentCalculator(
                Integer.parseInt(request.getParameter("kwota_kredyt")),
                Integer.parseInt(request.getParameter("raty_kredyt")),
                Integer.parseInt(request.getParameter("oplata_stala")),
                Integer.parseInt(request.getParameter("oprocentowanie_kredyt")),
                type
        );

        response.setContentType("text/html");
        if (type.equals("malejace")){
            System.out.println("malejące");
            response.getWriter().println(
                    buildLoanResponseString (installmentCalculator.calculateVariableRate(), type)
            );
        }
        else {
            System.out.println("stale");
            response.getWriter().println(
                    buildLoanResponseString (installmentCalculator.calculateFixedRate(), type)
            );
        }
	}

    // generowanie ciągu tekstowego z odpowiedzią w formacie html, na bazie tabeli z danymi
    private String buildLoanResponseString (float[][] loanTable, String type){
	    // head
	    String responseString = "<!DOCTYPE <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"><html lang=\"pl-PL\"><head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"><style>";
	    // CSS
        responseString += "table, td, th {border-collapse: collapse; border: 1px solid black; padding: 0.4em;}";
        responseString += "</style></head><body>";
        // body
        responseString += "<h1>Harmonogram spłat dla rat ";
        if(type.equals("malejace")){responseString += "malejących";}else{responseString += "stałych";}
        responseString += "</h1>";
        responseString += "<p><table>";
        responseString += "<tr>";
        responseString += "<th>Nr raty</th>";
        responseString += "<th>Kwota kapitału</th>";
        responseString += "<th>Kwota odsetek</th>";
        responseString += "<th>Opłaty stałe</th>";
        responseString += "<th>Całkowita kwota raty</th>";
        responseString += "</tr>";
        float totalCost = 0;
        System.out.println(loanTable.length);
        for (int i = 0; i < loanTable.length; i++){

            System.out.println(i);
            // dodawanie wierszy
            responseString += "<tr>";
            responseString += "<td>" + (i+1) + ".</td>";
            responseString += "<td>"+  convertToPLN(loanTable[i][0]) + "</td>";
            responseString += "<td>"+  convertToPLN(loanTable[i][1]) + "</td>";
            responseString += "<td>"+  convertToPLN(loanTable[i][2]) + "</td>";
            responseString += "<td>"+  convertToPLN(loanTable[i][3]) + "</td>";
            responseString += "</tr>";
            totalCost += loanTable[i][3];
        }
        responseString += "</table></p>";
        responseString += "<p>Całkowity koszt kredytu to: " + convertToPLN(totalCost) + "</p>";
        // button to home
        responseString += "<FORM></FORM>";
        responseString += "</body>";
        responseString += "<script>document.write('<a href=\"' + document.referrer + '\">Wróć do kalkulatora</a>');</script>";
	    return responseString;
    }

    // konwersja liczby do tekstowego formatu walutowego w zł
    private String convertToPLN(double amount){
	    return Math.round(amount*100.0)/100.0  +" zł";
    }

}
