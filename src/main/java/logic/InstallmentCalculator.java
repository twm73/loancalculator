package logic;

public class InstallmentCalculator {

    private float amount;
    private float term;
    private float cost;
    private float interest;
    private String type;

    public InstallmentCalculator(float amount, float term, float cost, float interest, String type) {
        this.amount = amount;
        this.term = term;
        this.cost = cost;
        this.interest = interest;
        this.type = type;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setTerm(float term) {
        this.term = term;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public void setInterest(float interest) {
        this.interest = interest;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float[][] calculateFixedRate(){
        float [][] tempTable = new float[(int)term][4];
        float adjustedRate = interest / 1200;

        float rateCostRatio = cost / (amount + cost);
        float rateAmountRatio =  amount / (amount + cost);
        double installment = (amount + cost)* adjustedRate * Math.pow(adjustedRate+1, term)/ (Math.pow(1+adjustedRate, term)-1);

        for (int i = 1; i <= term; i++) {

            double partialAmount = (amount + cost)* adjustedRate * Math.pow(adjustedRate+1, i-1)/ (Math.pow(1+adjustedRate, term)-1);

            tempTable [i-1][0] = (float)(partialAmount * rateAmountRatio);
            tempTable [i-1][1] = (float) (installment - partialAmount);
            tempTable [i-1][2] = (float)(partialAmount * rateCostRatio);
            tempTable [i-1][3] = (float) installment;
        }
        return tempTable;
    }
    public float[][] calculateVariableRate(){
        float [][] tempTable = new float[(int)term][4];
        float adjustedRate = interest / 100 / 12;
        float partialAmount = amount / term;
        float partialCost = cost / term;
        for (int i = 1; i <= term; i++) {
            float variableCost = (amount + cost) / term * (term - i + 1) * adjustedRate;
            tempTable [i-1][0] = partialAmount;
            tempTable [i-1][1] = variableCost;
            tempTable [i-1][2] = partialCost;
            tempTable [i-1][3] = partialAmount+partialCost+variableCost;
        }
        System.out.println("malejące");
        System.out.println(tempTable.length);
        return tempTable;
    }
}
